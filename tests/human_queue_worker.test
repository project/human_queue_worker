<?php

/**
 * @file
 * Contains tests for the Human queue worker module.
 */

/**
 * Base class containing helpers for Human Queue Worker tests.
 */
class HumanQueueWorkerTestBase extends DrupalWebTestCase {

  /**
   * Helper to create an entity and queue it.
   *
   * @param $queue_name
   *  The name of the queue to place the new entity into.
   *
   * @return
   *  The entity.
   */
  function helperEntityCreateQueue($queue_name) {
    $values =  array(
      'type' => 'alpha',
      'title' => $this->randomName(),
    );
    $entity = entity_create('entity_operations_test', $values);
    $entity->save();

    $queue = DrupalQueue::get($queue_name);
    $queue->createItem($entity->eid);

    return $entity;
  }

  /**
   * Assert that an entity is in a given queue.
   *
   * @param $entity
   *  The entity to check for. Must be an Entity API classes entity.
   * @param $queue_name
   *  The name of the queue to place the new entity into.
   * @param $message
   *  (optional) A message to display with the assertion.
   */
  protected function assertEntityInQueue($entity, $queue_name, $message = '') {
    $this->assertEntityInQueueHelper($entity, $queue_name, $message, TRUE);
  }

  /**
   * Assert that an entity is not in a given queue.
   *
   * @param $entity
   *  The entity to check for. Must be an Entity API classes entity.
   * @param $queue_name
   *  The name of the queue to place the new entity into.
   * @param $message
   *  (optional) A message to display with the assertion.
   */
  protected function assertEntityNotInQueue($entity, $queue_name, $message = '') {
    $this->assertEntityInQueueHelper($entity, $queue_name, $message, FALSE);
  }

  /**
   * Helper for assertEntityInQueue() and assertEntityNotInQueue().
   *
   * @param $entity
   *  The entity to check for. Must be an Entity API classes entity.
   * @param $queue_name
   *  The name of the queue to place the new entity into.
   * @param $message
   *  (optional) A message to display with the assertion.
   * @param $is_in_queue
   *  (optional) TRUE if the entity should be in the queue, false if it should
   *  not. Defaults to TRUE.
   */
  protected function assertEntityInQueueHelper($entity, $queue_name, $message = '', $is_in_queue = TRUE) {
    if (!$message) {
      $message = $is_in_queue ? "The entity is in the $queue_name queue." : "The entity is not in the $queue_name queue.";
    }
    $queue_items = db_query('SELECT item_id FROM {queue} q WHERE name = :name AND data = :data', array(
      ':name' => $queue_name,
      ':data' => serialize($entity->identifier()),
    ))->fetchAll();
    $this->assert(count($queue_items) == $is_in_queue, $message);
  }

}

/**
 * Base class for concurrent users.
 *
 * Copied from conflict module's ConflictWebTestCase.
 */
class HumanQueueWorkerConcurrentUsersTestBase extends DrupalWebTestCase {

  protected function drupalSwitchUser(stdClass $account) {
    if (isset($account->session)) {
      // Switch to the user session.
      $this->loggedInUser = $account;
      $this->curlHandle = $account->session->curlHandle;

      // Restore internal browser state of the user.
      // Updates content properties on $this as well as $this->loggedInUser.
      $this->drupalSetContent($account->session->content, $account->session->url);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Overrides DrupalWebTestCase::drupalLogin().
   *
   * @param object $user
   *   The user account to log in.
   * @param bool $create_concurrent_session
   *   (optional) Whether to create a new, concurrent user session. Pass TRUE
   *   for first login. Defaults to FALSE, which means that the currently logged
   *   in user, if logged in, is logged out and back in (default behavior).
   */
  protected function drupalLogin(stdClass $user, $create_concurrent_session = FALSE) {
    // If we're asked to create a new session, unset the current.
    if ($create_concurrent_session) {
      unset($this->curlHandle);
      $this->loggedInUser = FALSE;
    }
    // If there is a session attached on $user already, try to use that directly.
    elseif ($this->drupalSwitchUser($user)) {
      $this->verbose($this->drupalGetcontent());
      $this->pass(t('User %name is still logged in.', array('%name' => $user->name)), t('User login'));
      return;
    }

    // If we are logged in already and are asked to re-login, log out first.
    if ($this->loggedInUser && $this->loggedInUser->uid == $user->uid) {
      $this->drupalLogout();
    }

    $edit = array(
      'name' => $user->name,
      'pass' => $user->pass_raw
    );
    $this->drupalPost('user', $edit, t('Log in'));

    // If a "log out" link appears on the page, it is almost certainly because
    // the login was successful.
    $pass = $this->assertLink(t('Log out'), 0, t('User %name successfully logged in.', array('%name' => $user->name)), t('User login'));

    if ($pass) {
      // Save the user's session on the account itself.
      if (!isset($user->session)) {
        $user->session = new stdClass;
      }
      $user->session->curlHandle = $this->curlHandle;

      // Switch the currently logged in user.
      $this->loggedInUser = $user;
    }
  }

  /**
   * Overrides DrupalWebTestCase::drupalLogout().
   *
   * @param object $account
   *   (optional) The user account to log out.
   */
  protected function drupalLogout(stdClass $account = NULL) {
    // If a specific account was passed, ensure that we are operating on that
    // user's session first.
    if (isset($account) && (!$this->loggedInUser || $account->uid != $this->loggedInUser)) {
      $this->drupalSwitchUser($account);
    }

    // Make a request to the logout page, and redirect to the user page, the
    // idea being if you were properly logged out you should be seeing a login
    // screen.
    $this->drupalGet('user/logout');
    $this->drupalGet('user');
    $pass = $this->assertField('name', t('Username field found.'), t('Logout'));
    $pass = $pass && $this->assertField('pass', t('Password field found.'), t('Logout'));

    if ($pass) {
      // Remove the user's session on the account itself.
      if (isset($account->session->curlHandle)) {
        curl_close($account->session->curlHandle);
      }
      unset($account->session);

      // Switch to no session.
      $this->loggedInUser = FALSE;
    }
  }

  /**
   * Overrides DrupalWebTestCase::drupalSetContent().
   */
  protected function drupalSetContent($content, $url = 'internal:') {
    parent::drupalSetContent($content, $url);

    // Clone the relevant results onto the currently logged in user
    // account/session.
    if (isset($this->loggedInUser->session)) {
      $this->loggedInUser->session->url = $this->url;
      $this->loggedInUser->session->content = $this->content;
      $this->loggedInUser->session->drupalSettings = $this->drupalSettings;
    }
  }
}

/**
 * Test case for access control to the UI.
 */
class HumanQueueWorkerUIAccessTestCase extends DrupalWebTestCase {

  /**
   * Implements getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => t('UI access'),
      'description' => t('Test access control to the queue processing UI.'),
      'group' => t('Human Queue Worker'),
    );
  }

  /**
   * Implements setUp().
   */
  function setUp() {
    // Call the parent with an array of modules to enable for the test.
    parent::setUp(array(
      'human_queue_worker',
      // Provides our test entity, which already has some operations on it we
      // can use.
      'entity_operations_test',
      // Adds further operations specific to queue working.
      'human_queue_worker_test',
    ));
  }

  function testUIAccess() {
    $queue_name = 'test_queue_alpha';
    $path = "queue_processing/$queue_name";

    // Create our users.
    // Note that we don't need to grant access to specific operations on our
    // test entity, as entity_operations_test.module doesn't have any kind of
    // access control... yet!
    // User with no access.
    $this->unprivilegedUser = $this->drupalCreateUser();
    // User with access to one queue.
    $this->privilegedUser = $this->drupalCreateUser(array("process queue $queue_name"));
    // User with access to all the queues.
    $this->superUser = $this->drupalCreateUser(array("process all human queues"));

    $this->drupalLogin($this->unprivilegedUser);

    $this->drupalGet('queue_processing');
    $this->assertResponse(403, "User with no access cannot access the queue overview page.");
    $this->drupalGet($path);
    $this->assertResponse(403, "User with no access cannot access the queue processing page.");

    $this->drupalLogin($this->privilegedUser);

    $this->drupalGet('queue_processing');
    $this->assertResponse(200, "User with access to the queue can access the queue overview page.");
    $this->assertText('Test queue alpha', "The test queue title is shown.");

    $this->drupalGet($path);
    $this->assertResponse(200, "User with access to the queue can access the queue processing page.");
    $this->assertText('Test queue alpha', "The test queue title is shown.");

    $this->drupalLogin($this->superUser);

    $this->drupalGet('queue_processing');
    $this->assertResponse(200, "User with access to all queues can access the queue overview page.");
    $this->assertText('Test queue alpha', "The test queue title is shown.");

    $this->drupalGet($path);
    $this->assertResponse(200, "User with access to all queues can access the queue processing page.");
    $this->assertText('Test queue alpha', "The test queue title is shown.");
  }

}

/**
 * Test case for basic operations with a single user.
 */
class HumanQueueWorkerOperationsTestCase extends HumanQueueWorkerTestBase {

  /**
   * Implements getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => t('Basic operations'),
      'description' => t('Test operation of a human queue with a single worker.'),
      'group' => t('Human Queue Worker'),
    );
  }

  /**
   * Implements setUp().
   */
  function setUp() {
    // Call the parent with an array of modules to enable for the test.
    parent::setUp(array(
      'human_queue_worker',
      // Provides our test entity, which already has some operations on it we
      // can use.
      'entity_operations_test',
      // Adds further operations specific to queue working.
      'human_queue_worker_test',
    ));

    $queue_name = 'test_queue_alpha';

    // Create and log in a web user.
    $this->user = $this->drupalCreateUser(array("process queue $queue_name"));
    $this->drupalLogin($this->user);
  }

  /**
   * Test the module's functionality.
   */
  function testQueueWorkerForm() {
    $queue_name = 'test_queue_alpha';
    $path = "queue_processing/$queue_name";
    $queue = DrupalQueue::get($queue_name);

    // Create a single entity and queue it.
    $entity = $this->helperEntityCreateQueue($queue_name);

    $this->drupalGet($path);

    $expected_string = t("Current item is '@label' (!id).", array(
      '@label' => $entity->label(),
      '!id' => $entity->identifier(),
    ));
    $this->assertText($expected_string, "The queued entity is shown for processing.");

    $this->drupalGet($path);
    $this->assertText($expected_string, "The same queued entity is shown for processing when the user reloads the form.");

    // Click the button for the 'red' operation.
    $this->drupalPost(NULL, array(), t('Make it red'));

    // Check the entity was acted upon.
    entity_get_controller('entity_operations_test')->resetCache();
    $entity = entity_load_single('entity_operations_test', $entity->eid);

    $this->assertEqual($entity->title, 'Red', "The entity title is changed to 'Red'.");

    // The queue should now be empty.
    $this->assertEqual($queue->numberOfItems(), 0, "The queue is empty.");
    $this->assertText(t("No items in the @queue queue are available for processing.", array('@queue' => 'Test queue alpha')), "The page shows a message when no queue items remain.");

    // Create and queue more entities.
    $entity_1 = $this->helperEntityCreateQueue($queue_name);
    $entity_2 = $this->helperEntityCreateQueue($queue_name);
    $entity_3 = $this->helperEntityCreateQueue($queue_name);
    $entity_4 = $this->helperEntityCreateQueue($queue_name);
    $entity_5 = $this->helperEntityCreateQueue($queue_name);
    $entity_6 = $this->helperEntityCreateQueue($queue_name);

    $this->drupalGet($path);

    $expected_string = t("Current item is '@label' (!id).", array(
      '@label' => $entity_1->label(),
      '!id' => $entity_1->identifier(),
    ));
    $this->assertText($expected_string, "The queued entity is shown for processing.");

    // Click the button for the 'blue' operation.
    $this->drupalPost(NULL, array(), t('Make it blue'));

    // Check the entity was acted upon.
    entity_get_controller('entity_operations_test')->resetCache();
    $entity_1 = entity_load_single('entity_operations_test', $entity_1->eid);

    $this->assertEqual($entity_1->title, 'Blue', "The entity title is changed to 'Blue'.");

    // Check the item is no longer in the queue.
    $this->assertEntityNotInQueue($entity_1, $queue_name, "The entity is no longer in the queue.");

    // Test next queue item shows.
    $expected_string = t("Current item is '@label' (!id).", array(
      '@label' => $entity_2->label(),
      '!id' => $entity_2->identifier(),
    ));
    $this->assertText($expected_string, "The next queued entity is shown for processing.");

    // Use the multistep form item.
    $this->drupalPost(NULL, array(), t('Search for data'));

    // Check the item is still in the queue.
    $this->assertEntityInQueue($entity_2, $queue_name, "The entity is still in the queue after the form's first step was submitted.");

    // Check the radio buttons are now shown in the multistep operation form.
    $this->assertText('Rho', "The new option is shown.");

    $edit = array(
      'lookup_data' => 'Rho',
    );
    $this->drupalPost(NULL, $edit, t('Execute operation'));

    // Check the entity was acted upon.
    entity_get_controller('entity_operations_test')->resetCache();
    $entity_2 = entity_load_single('entity_operations_test', $entity_2->eid);

    $this->assertEqual($entity_2->title, 'Rho', "The entity title is changed to 'Rho'.");

    // Check the item is no longer in the queue.
    $this->assertEntityNotInQueue($entity_2, $queue_name, "The entity is no longer in the queue.");

    // Test next queue item shows.
    $expected_string = t("Current item is '@label' (!id).", array(
      '@label' => $entity_3->label(),
      '!id' => $entity_3->identifier(),
    ));
    $this->assertText($expected_string, "The next queued entity is shown for processing.");

    // Delete this entity.
    $edit = array(
      'confirm' => TRUE,
    );
    $this->drupalPost(NULL, $edit, t('Delete Test Entity'));

    // Check the item is no longer in the queue.
    // (Do this before we test the entity was deleted, as once we do that we
    // won't have it any more.)
    $this->assertEntityNotInQueue($entity_3, $queue_name, "The entity is no longer in the queue.");

    $this->assertText('has been deleted', "A message is shown confirming that the entity has been deleted.");
    entity_get_controller('entity_operations_test')->resetCache();
    $entity_3 = entity_load_single('entity_operations_test', $entity_3->eid);
    $this->assertFalse($entity_3, "The entity has been deleted.");

    // Test next queue item shows.
    $expected_string = t("Current item is '@label' (!id).", array(
      '@label' => $entity_4->label(),
      '!id' => $entity_4->identifier(),
    ));
    $this->assertText($expected_string, "The next queued entity is shown for processing.");

    // Skip this entity.
    $this->drupalPost(NULL, array(), t('Skip this item'));

    // To test the entity was re-queued at the end of the queue, we need to hack
    // into the queue's table directly.
    $queue_items = db_query('SELECT data, item_id FROM {queue} q WHERE name = :name ORDER BY created ASC', array(
      ':name' => $queue_name,
    ))->fetchAll();
    //debug($queue_items);

    // The last item in the queue should be the one we just skipped.
    $skipped_queue_item = array_pop($queue_items);
    $skipped_queue_item_data = unserialize($skipped_queue_item->data);
    $this->assertEqual($skipped_queue_item_data, $entity_4->identifier(), "The entity was re-queued and is now at the end of the queue.");

    // Test next queue item shows.
    $expected_string = t("Current item is '@label' (!id).", array(
      '@label' => $entity_5->label(),
      '!id' => $entity_5->identifier(),
    ));
    $this->assertText($expected_string, "The next queued entity is shown for processing.");

    // Move this item to another queue.
    $this->drupalPost(NULL, array(), t("Move item to 'Test queue beta' queue"));

    // Check the item is no longer in the queue.
    $this->assertEntityNotInQueue($entity_5, $queue_name, "The entity is no longer in the queue.");

    // Check the item is now in the beta queue.
    $this->assertEntityInQueue($entity_5, 'test_queue_beta', "The entity is now in the beta queue.");

    // Test next queue item shows.
    $expected_string = t("Current item is '@label' (!id).", array(
      '@label' => $entity_6->label(),
      '!id' => $entity_6->identifier(),
    ));
    $this->assertText($expected_string, "The next queued entity is shown for processing.");

    // Remove this entity from the queue.
    $this->drupalPost(NULL, array(), t('Remove from the queue'));

    // Check the item is no longer in the queue.
    $this->assertEntityNotInQueue($entity_6, $queue_name, "The entity was removed from the queue.");
  }

}

/**
 * Test case for concurrent users.
 */
class HumanQueueWorkerConcurrentUsersTestCase extends HumanQueueWorkerConcurrentUsersTestBase {

  /**
   * Implements getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => t('Concurrent users'),
      'description' => t('Test multiple concurrent users working on the queue.'),
      'group' => t('Human Queue Worker'),
    );
  }

  /**
   * Implements setUp().
   */
  function setUp() {
    // Call the parent with an array of modules to enable for the test.
    parent::setUp(array(
      'human_queue_worker',
      // Provides our test entity, which already has some operations on it we
      // can use.
      'entity_operations_test',
      // Adds further operations specific to queue working.
      'human_queue_worker_test',
    ));

    $queue_name = 'test_queue_alpha';

    // Create two web users.
    $this->user1 = $this->drupalCreateUser(array("process queue $queue_name"));
    $this->user2 = $this->drupalCreateUser(array("process queue $queue_name"));
  }

  /**
   * Helper to create an entity and queue it.
   *
   * @param $queue_name
   *  The name of the queue to place the new entity into.
   *
   * @return
   *  The entity.
   */
  function helperEntityCreateQueue($queue_name) {
    $values =  array(
      'type' => 'alpha',
      'title' => $this->randomName(),
    );
    $entity = entity_create('entity_operations_test', $values);
    $entity->save();

    $queue = DrupalQueue::get($queue_name);
    $queue->createItem($entity->eid);

    return $entity;
  }

  /**
   * Test the module's functionality.
   */
  function testQueueWorkerConcurrentUsers() {
    $queue_name = 'test_queue_alpha';
    $path = "queue_processing/$queue_name";
    $queue = DrupalQueue::get($queue_name);

    // Create a single entity and queue it.
    $entity = $this->helperEntityCreateQueue($queue_name);

    // Login first user.
    $this->drupalLogin($this->user1);

    $this->drupalGet($path);

    $expected_string = t("Current item is '@label' (!id).", array(
      '@label' => $entity->label(),
      '!id' => $entity->identifier(),
    ));
    $this->assertText($expected_string, "The queued entity is shown for processing to the first user.");

    // Login second user.
    // Without passing TRUE, the first user would be logged out.
    $this->drupalLogin($this->user2, TRUE);

    $this->drupalGet($path);

    $this->assertText(t("No items in the @queue queue are available for processing.", array('@queue' => 'Test queue alpha')), "The second user is shown a message that no items remain in the queue.");

    // Return to the first user.
    // (Confusingly, we DON'T pass TRUE here to keep user 2 logged in.)
    $this->drupalLogin($this->user1);

    $this->drupalGet($path);

    // The user should see the same item, despite having reloaded the form.
    $expected_string = t("Current item is '@label' (!id).", array(
      '@label' => $entity->label(),
      '!id' => $entity->identifier(),
    ));
    $this->assertText($expected_string, "The first user is shown the entity they had claimed on the previous page load.");

    // Click the button for the 'blue' operation.
    $this->drupalPost(NULL, array(), t('Make it blue'));

    // We don't check the result of the operation here; that's tested in the
    // basic test case.

    // Queue multiple items.
    $entity_1 = $this->helperEntityCreateQueue($queue_name);
    $entity_2 = $this->helperEntityCreateQueue($queue_name);
    $entity_3 = $this->helperEntityCreateQueue($queue_name);
    $entity_4 = $this->helperEntityCreateQueue($queue_name);

    // The two users should see each a different item.
    $this->drupalGet($path);

    $expected_string = t("Current item is '@label' (!id).", array(
      '@label' => $entity_1->label(),
      '!id' => $entity_1->identifier(),
    ));
    $this->assertText($expected_string, "The 1st queued entity is shown for processing to the first user.");

    // Switch to the second user.
    $this->drupalLogin($this->user2);

    $this->drupalGet($path);

    $expected_string = t("Current item is '@label' (!id).", array(
      '@label' => $entity_2->label(),
      '!id' => $entity_2->identifier(),
    ));
    $this->assertText($expected_string, "The 2nd queued entity is shown for processing to the first user.");

    // Switch to first user.
    $this->drupalLogin($this->user1);

    // Click the button for the 'blue' operation.
    $this->drupalGet($path);
    $this->drupalPost(NULL, array(), t('Make it blue'));

    // The first user should see a new item for entity 3, as entity 2 is
    // currently claimed by the second user.
    $expected_string = t("Current item is '@label' (!id).", array(
      '@label' => $entity_3->label(),
      '!id' => $entity_3->identifier(),
    ));
    $this->assertText($expected_string, "The 3rd queued entity is shown for processing to the first user.");

    // Switch to the second user.
    $this->drupalLogin($this->user2);

    // Click the button for the 'blue' operation.
    $this->drupalGet($path);
    $this->drupalPost(NULL, array(), t('Make it blue'));

    // The first user should see a new item for entity 3, as entity 2 is
    // currently claimed by the second user.
    $expected_string = t("Current item is '@label' (!id).", array(
      '@label' => $entity_4->label(),
      '!id' => $entity_4->identifier(),
    ));
    $this->assertText($expected_string, "The 4th queued entity is shown for processing to the second user.");
  }

}

/**
 * Test case for our base class's assertions.
 */
class HumanQueueWorkerTestBaseTestCase extends HumanQueueWorkerTestBase {

  /**
   * Implements getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => t('Concurrent users'),
      'description' => t('Test multiple concurrent users working on the queue.'),
      'group' => t('Human Queue Worker'),
    );
  }

  /**
   * Implements setUp().
   */
  function setUp() {
    // Call the parent with an array of modules to enable for the test.
    parent::setUp(array(
      'human_queue_worker',
      // Provides our test entity, which already has some operations on it we
      // can use.
      'entity_operations_test',
      // Note we don't need our queue operations here; we're just checking
      // things are in a queue or not.
    ));
  }

  /**
   * Test our queue assertions.
   */
  function testQueueAssertions() {
    $queue_name = 'test_queue_alpha';

    // Create a new entity and queue it.
    $values =  array(
      'type' => 'alpha',
      'title' => $this->randomName(),
    );
    $entity = entity_create('entity_operations_test', $values);
    $entity->save();

    $queue = DrupalQueue::get($queue_name);
    $queue->createItem($entity->eid);

    $this->assertEntityInQueue($entity, $queue_name);

    // Check that having the entity repeated in the queue works too.
    $queue->createItem($entity->eid);
    $this->assertEntityInQueue($entity, $queue_name);

    // Create a new entity and don't queue it.
    $values =  array(
      'type' => 'alpha',
      'title' => $this->randomName(),
    );
    $entity = entity_create('entity_operations_test', $values);
    $entity->save();

    $this->assertEntityNotInQueue($entity, $queue_name);
  }

}
